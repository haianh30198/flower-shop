const User = require('../models/userModel')

const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');

exports.signup = async(req, res) => {
    try {
        if (req.body.password !== req.body.passwordConfirm) {
            return res.status(400).json({
                status: 'error',
                message: 'Passwords are not matching'
            })
        }

        if (req.body.username) {
            const user = await User.findOne({
                username: req.body.username
            })

            if (user) {
                return res.status(400).json({
                    status: 'error',
                    message: 'Username already exists'
                })
            }
        }

        if (req.body.email) {
            const user = await User.findOne({
                email: req.body.email
            })

            if (user) {
                return res.status(400).json({
                    status: 'error',
                    message: 'Email already exists'
                })
            }
        }

        const hashPassword = await bcrypt.hash(req.body.password, 12);

        const user = await User.create({
            name: req.body.name,
            username: req.body.username,
            email: req.body.email,
            password: hashPassword,
            passwordConfirm: req.body.passwordConfirm,
            phone: req.body.phone,
            birthday: req.body.birthday,
            address: req.body.address
        })

        const token = jwt.sign({ data: user._id }, 'HaianhNguyen', { expiresIn: '7d' })

        user.passwordConfirm = undefined;

        await user.save();

        return res.status(201).json({
            token,
            status: 'success',
            data: user
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.signin = async(req, res) => {
    try {
        const user = await User.findOne({ username: req.body.username });

        if (!user) {
            return res.status(404).json({
                status: 'error',
                message: 'Password or username is incorrect'
            })
        }

        const isMatched = await bcrypt.compare(req.body.password, user.password);

        if (!isMatched) {
            return res.status(404).json({
                status: 'error',
                message: 'Password or username is incorrect'
            })
        }

        const token = jwt.sign({ data: user._id }, 'HaianhNguyen', { expiresIn: '7d' })

        res.status(200).json({
            token,
            status: 'success',
            message: 'Signed in successfully',
            data: user
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.signout = async(req, res) => {
    try {
        if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer')) {
            return res.status(404).json({
                status: 'fail',
                message: 'You are not logged in'
            })
        }

        var token = req.headers.authorization.split(' ')[1]

        const decoded = jwt.verify(token, 'HaianhNguyen')

        const user = await User.findOne({
            _id: decoded.data
        });

        var token = jwt.sign({ data: user._id }, 'HaianhNguyen', { expiresIn: '0s' })

        res.status(200).json({
            status: 'success',
            message: 'Signed Out Successfully'
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.authenticate = async(req, res, next) => {
    try {
        if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer')) {
            return res.status(404).json({
                status: 'error',
                message: 'You are not logged in'
            })
        }

        const token = req.headers.authorization.split(' ')[1];

        const decoded = jwt.verify(token, 'HaianhNguyen')

        const user = await User.findOne({
            _id: decoded.data
        });

        if (!user) {
            return res.status(403).json({
                status: 'error',
                message: 'User belong to token has been deleted'
            })
        }

        //Dán req.user bầng usẻr để những middleware sau dùng
        req.user = user

        next()
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.restrictTo = (...roles) => {
    return ((req, res, next) => {
        if (roles.every(role => role != req.user.role)) {
            return res.status(401).json({
                status: 'error',
                message: 'You have no permission to access this route'
            })
        }

        next()
    })
}

exports.forgotPassword = async(req, res) => {
    try {
        const user = await User.findOne({
            email: req.body.email
        })

        if (!user) {
            return res.status(404).json({
                status: 'error',
                message: 'User not found with that email'
            })
        }

        const token = jwt.sign({ data: user._id }, 'forgotPassword', { expiresIn: '30m' })

        "use strict";
        const nodemailer = require("nodemailer");

        // async..await is not allowed in global scope, must use a wrapper
        async function main() {
            // Generate test SMTP service account from ethereal.email
            // Only needed if you don't have a real mail account for testing
            // let testAccount = await nodemailer.createTestAccount();

            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
                service: "gmail",
                port: 587,
                secure: true, // true for 465, false for other ports
                auth: {
                    user: 'bot.haianh@gmail.com', // generated ethereal user
                    pass: 'bothaianh123', // generated ethereal password
                },
            });

            // send mail with defined transport object
            let info = await transporter.sendMail({
                from: '"Haianh Nguyen 👻" <bot.haianh@gmail.com>', // sender address
                to: req.body.email, // list of receivers
                subject: "[NO-REPLY] - Đổi mật khẩu ✔", // Subject line
                text: `Chào ${user.name} gần đây bạn có cần đổi mật khẩu phải không !`, // plain text body
                html: `
<style>
body {
margin: 0;
padding: 0;
mso-line-height-rule: exactly;
min-width: 100%;
}

.wrapper {
display: table;
table-layout: fixed;
width: 100%;
min-width: 620px;
-webkit-text-size-adjust: 100%;
-ms-text-size-adjust: 100%;
}

body,
.wrapper {
background-color: #ffffff;
}
/* Basic */

table {
border-collapse: collapse;
border-spacing: 0;
}

table.center {
margin: 0 auto;
width: 602px;
}

td {
padding: 0;
vertical-align: top;
}

.spacer,
.border {
font-size: 1px;
line-height: 1px;
}

.spacer {
width: 100%;
line-height: 16px
}

.border {
background-color: #e0e0e0;
width: 1px;
}

.padded {
padding: 0 24px;
}

img {
border: 0;
-ms-interpolation-mode: bicubic;
}

.image {
font-size: 12px;
}

.image img {
display: block;
}

strong,
.strong {
font-weight: 700;
}

h1,
h2,
h3,
p,
ol,
ul,
li {
margin-top: 0;
}

ol,
ul,
li {
padding-left: 0;
}

a {
text-decoration: none;
color: #616161;
}

.btn {
background-color: #2196F3;
border: 1px solid #2196F3;
border-radius: 2px;
color: #ffffff;
display: inline-block;
font-family: Roboto, Helvetica, sans-serif;
font-size: 14px;
font-weight: 400;
line-height: 36px;
text-align: center;
text-decoration: none;
text-transform: uppercase;
width: 200px;
height: 36px;
padding: 0 8px;
margin: 0;
outline: 0;
outline-offset: 0;
-webkit-text-size-adjust: none;
mso-hide: all;
}
/* Top panel */

.title {
text-align: left;
}

.subject {
text-align: right;
}

.title,
.subject {
width: 300px;
padding: 8px 0;
color: #616161;
font-family: Roboto, Helvetica, sans-serif;
font-weight: 400;
font-size: 12px;
line-height: 14px;
}
/* Header */

.logo {
padding: 16px 0;
}
/* Logo */

.logo-image {}
/* Main */

.main {
-webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
-moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
}
/* Content */

.columns {
margin: 0 auto;
width: 600px;
background-color: #ffffff;
font-size: 14px;
}

.column {
text-align: left;
background-color: #ffffff;
font-size: 14px;
}

.column-top {
font-size: 24px;
line-height: 24px;
}

.content {
width: 100%;
}

.column-bottom {
font-size: 8px;
line-height: 8px;
}

.content h1 {
margin-top: 0;
margin-bottom: 16px;
color: #212121;
font-family: Roboto, Helvetica, sans-serif;
font-weight: 400;
font-size: 20px;
line-height: 28px;
}

.content p {
margin-top: 0;
margin-bottom: 16px;
color: #212121;
font-family: Roboto, Helvetica, sans-serif;
font-weight: 400;
font-size: 16px;
line-height: 24px;
}

.content .caption {
color: #616161;
font-size: 12px;
line-height: 20px;
}
/* Footer */

.signature,
.subscription {
vertical-align: bottom;
width: 300px;
padding-top: 8px;
margin-bottom: 16px;
}

.signature {
text-align: left;
}

.subscription {
text-align: right;
}

.signature p,
.subscription p {
margin-top: 0;
margin-bottom: 8px;
color: #616161;
font-family: Roboto, Helvetica, sans-serif;
font-weight: 400;
font-size: 12px;
line-height: 18px;
}
</style>

<center class="wrapper">
<table class="top-panel center" width="602" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="title" width="300">Haianh Nguyen</td>
<td class="subject" width="300"><a class="strong" href="#" target="_blank">www.haianh.ml</a></td>
</tr>
<tr>
<td class="border" colspan="2">&nbsp;</td>
</tr>
</tbody>
</table>

<div class="spacer">&nbsp;</div>

<table class="main center" width="602" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="column">
    <div class="column-top">&nbsp;</div>
    <table class="content" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td class="padded">
                    <h1>Chào ${user.name},</h1>
                    <p>Gần đây bạn vô tình quên mật khẩu tại <strong>Flower Shop</strong> đúng không.</p>
                    <p>Nếú bạn có ý định thay đổi mật khẩu thì có thể nhấn vào nút bên dưới chúng tôi sẽ giúp bạn thay đổi mật khẩu nhé !.</p>
                    <p>Còn nếu bạn không quên thì có thể bỏ qua email này.</p>
                    <p>Hoặc nghi ngờ có người muốn chôm tài khoản này của bạn, nhấn vào bên dưới nhé !:</p>
                    <p style="text-align:center;"><a href="http://localhost:3000/api/users/change-password?token=${token}" class="btn">CHANGE PASSWORD</a></p>
                    <p style="text-align:center;">
                        <a href="http://localhost:3000/api/users/change-password?token=${token}" class="strong">http://haianh.ml</a>
                    </p>
                    <p class="caption">Chúng tôi luôn đảm bảo thông tin của bạn là riêng tư.</p>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="column-bottom">&nbsp;</div>
</td>
</tr>
</tbody>
</table>

<div class="spacer">&nbsp;</div>

<table class="footer center" width="602" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="border" colspan="2">&nbsp;</td>
</tr>
<tr>
<td class="signature" width="300">
    <p>
        Chào bạn nhé,<br> Flower Shop<br> 033 58 321 68, Haianh Nguyen<br>
    </p>
    <p>
        Hỗ trợ: <a class="strong" href="mailto:#" target="_blank">haianh30198@gmail.com</a>
    </p>
</td>
<td class="subscription" width="300">
    <div class="logo-image">
        <a href="https://zavoloklom.github.io/material-design-iconic-font/" target="_blank"><img src="https://zavoloklom.github.io/material-design-iconic-font/icons/mstile-70x70.png" alt="logo-alt" width="70" height="70"></a>
    </div>
    <p>
        <a class="strong block" href="#" target="_blank">
            Liên hệ hợp tác
        </a>
        <span class="hide">&nbsp;&nbsp;|&nbsp;&nbsp;</span>
        <a class="strong block" href="#" target="_blank">
            Cài đặt tài khoản
        </a>
    </p>
</td>
</tr>
</tbody>
</table>
</center>
                `, // html body
            });

            console.log("Message sent: %s", info.messageId);
            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

            // Preview only available when sending through an Ethereal account
            console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        }

        main().catch(console.error);

        res.status(200).json({
            status: 'success',
            message: 'We sent you an email, please check your email'
        })
    } catch (err) {
        console.log(err)
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.changePassword = async(req, res) => {
    try {
        const decoded = jwt.verify(req.query.token, 'forgotPassword')

        const user = await User.findOne({
            _id: decoded.data
        });

        if (!user) {
            return res.status(404).json({
                status: 'error',
                message: 'Your token has expired'
            })
        }

        if (req.body.newPassword !== req.body.newPasswordConfirm) {
            return res.status(400).json({
                status: 'error',
                message: 'Passwords are not matching'
            })
        }

        const hashPassword = await bcrypt.hash(req.body.newPassword, 12);

        await User.findByIdAndUpdate(user._id, {
            password: hashPassword,
            runValidators: true
        })

        res.status(200).json({
            status: 'success',
            message: 'Change Password successfully'
        })
    } catch (err) {
        console.log(err)
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}