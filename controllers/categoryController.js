const Category = require('../models/categoryModel');

const cloudinary = require('cloudinary').v2
const fs = require('fs')

cloudinary.config({
    cloud_name: 'vulythienanh',
    api_key: '711852792357315',
    api_secret: 'zxPG9EpsCwqF4n1bRV_pGV4vkbQ'
})

exports.getAllCategories = async(req, res) => {
    try {
        //Phân trang (pagination)
        if (!req.query.page) {
            var category = await Category.find();
        } else {
            var n = req.query.page
            var x = 2 //Số lượng sp trên 1 trang
            category = await Category.find().skip(n * x - x).limit(x)
        }
        //End--Phân trang (pagination)

        //Tìm kiếm
        var search = await Category.find({ name: { $regex: ".*" + req.query.search + ".*", $options: 'i' } });

        res.status(200).json({
            status: 'success',
            next: !req.query.page ? null : (category == "" ? null : `http://localhost:3000/api/categories?page=${parseInt(n)+1}`),
            previous: !req.query.page ? null : (parseInt(n) - 1 == 0 ? null : `http://localhost:3000/api/categories?page=${parseInt(n)-1}`),
            page: req.query.page,
            results: category.length,
            data: category,
            search
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.createCategory = async(req, res) => {
    try {
        if (req.files) {
            //Kiểm tra định dạng file upload
            if (!req.files.imageCover.mimetype.startsWith('image/')) {
                return res.status(404).json({
                    status: 'error',
                    message: 'Image Cover must be an image'
                })
            }

            //Upload hình lên cloudinary
            const file = req.files.imageCover;
            var uploadImgCover = await cloudinary.uploader.upload(file.tempFilePath, function(err, result) {
                console.log("Error", err);
                console.log("Success", result);
            })

            //Dán link sau khi upload vào req.body để lưu vào database
            if (uploadImgCover) {
                req.body.imageCover = uploadImgCover.secure_url
            }

            //Xóa file temp trong tmp
            fs.unlinkSync(req.files.imageCover.tempFilePath)
        }

        const category = await Category.create(req.body);

        res.status(201).json({
            status: 'success',
            data: category
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.getCategory = async(req, res) => {
    try {
        const category = await Category.findById(req.params.id);

        if (!category) {
            return res.status(404).json({
                status: 'error',
                message: 'Category not found with that ID'
            })
        }

        res.status(200).json({
            status: 'success',
            data: category
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.updateCategory = async(req, res) => {
    try {
        if (req.files) {
            //Kiểm tra định dạng file upload
            if (!req.files.imageCover.mimetype.startsWith('image/')) {
                return res.status(404).json({
                    status: 'error',
                    message: 'Image Cover must be an image'
                })
            }

            //Upload hình lên cloudinary
            const file = req.files.imageCover;
            var uploadImgCover = await cloudinary.uploader.upload(file.tempFilePath, function(err, result) {
                console.log("Error", err);
                console.log("Success", result);
            })

            //Dán link sau khi upload vào req.body để lưu vào database
            if (uploadImgCover) {
                req.body.imageCover = uploadImgCover.secure_url
            }

            //Xóa file temp trong tmp
            fs.unlinkSync(req.files.imageCover.tempFilePath)
        }

        const category = await Category.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        })

        if (!category) {
            return res.status(404).json({
                status: 'error',
                message: 'Category not found with that ID'
            })
        }

        res.status(200).json({
            status: 'success',
            data: category
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.deleteCategory = async(req, res) => {
    try {
        const category = await Category.findByIdAndDelete(req.params.id);

        if (!category) {
            return res.status(404).json({
                status: 'error',
                message: 'Category not found with that ID'
            })
        }

        return res.status(204).json({
            status: 'sucess',
            data: null
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}