const Cart = require('../models/cartModel');

exports.getAllCarts = async(req, res) => {
    try {
        //Phân trang (pagination)
        if (!req.query.page) {
            var cart = await Cart.find();
        } else {
            var n = req.query.page
            var x = 2 //Số lượng sp trên 1 trang
            cart = await Cart.find().skip(n * x - x).limit(x)
        }
        //End--Phân trang (pagination)

        //Tìm kiếm
        var search = await Cart.find({ name: { $regex: ".*" + req.query.search + ".*", $options: 'i' } });

        res.status(200).json({
            status: 'success',
            next: !req.query.page ? null : (cart == "" ? null : `http://localhost:3000/api/carts?page=${parseInt(n)+1}`),
            previous: !req.query.page ? null : (parseInt(n) - 1 == 0 ? null : `http://localhost:3000/api/carts?page=${parseInt(n)-1}`),
            page: req.query.page,
            results: cart.length,
            data: cart,
            search
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Some things went wrong'
        })
    }
}

exports.createCart = async(req, res) => {
    try {
        const cart = await Cart.create(req.body);

        res.status(201).json({
            status: 'success',
            data: cart
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Some things went wrong'
        })
    }
}

exports.getCart = async(req, res) => {
    try {
        const cart = await Cart.findById(req.params.id);

        if (!cart) {
            return res.status(404).json({
                status: 'error',
                message: 'Cart not found with that ID'
            })
        }

        res.status(200).json({
            status: 'success',
            data: cart
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Some things went wrong'
        })
    }
}

exports.updateCart = async(req, res) => {
    try {
        const cart = await Cart.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        })

        if (!cart) {
            return res.status(404).json({
                status: 'error',
                message: 'Cart not found with that ID'
            })
        }

        res.status(200).json({
            status: 'success',
            data: cart
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Some things went wrong'
        })
    }
}

exports.deleteCart = async(req, res) => {
    try {
        const cart = await Cart.findByIdAndDelete(req.params.id);

        if (!cart) {
            return res.status(404).json({
                status: 'error',
                message: 'Cart not found with that ID'
            })
        }

        res.status(204).json({
            status: 'success',
            data: null
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Some things went wrong'
        })
    }
}