const CartItem = require('../models/cartItemModel')
const Product = require('../models/productModel')

exports.getAllCartItems = async(req, res) => {
    try {
        const cartItem = await CartItem.find();

        res.status(200).json({
            status: 'success',
            results: cartItem.length,
            data: cartItem
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.createCartItem = async(req, res) => {
    try {
        const product = await Product.findById(req.body.productID);

        if (!product) {
            return res.status(404).json({
                status: 'error',
                message: 'Product not found with that ID'
            })
        }

        const cartItem = await CartItem.create(req.body);

        res.status(201).json({
            status: 'success',
            data: cartItem
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.getCartItem = async(req, res) => {
    try {
        const cartItem = await CartItem.findById(req.params.id);

        if (!cartItem) {
            return res.status(404).json({
                status: 'error',
                message: 'Cart item not found with that ID'
            })
        }

        res.status(200).json({
            status: 'success',
            date: cartItem
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.updateCartItem = async(req, res) => {
    try {
        const cartItem = await CartItem.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        })

        if (!cartItem) {
            return res.status(404).json({
                status: 'error',
                message: 'Cart item not found with that ID'
            })
        }

        res.status(201).json({
            status: 'success',
            date: cartItem
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.deleteCartItem = async(req, res) => {
    try {
        const cartItem = await CartItem.findByIdAndDelete(req.params.id);

        if (!cartItem) {
            return res.status(404).json({
                status: 'error',
                message: 'Cart item not found with that ID'
            })
        }

        res.status(204).json({
            status: 'success',
            date: null
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}