const Order = require('../models/orderModel');
const OrderItems = require('../models/orderItemModel')
const Product = require('../models/productModel')

exports.getAllOrders = async(req, res) => {
    try {
        //Phân trang (pagination)
        if (!req.query.page) {
            var order = await Order.find();
        } else {
            var n = req.query.page
            var x = 2 //Số lượng sp trên 1 trang
            order = await Order.find().skip(n * x - x).limit(x)
        }
        //End--Phân trang (pagination)

        //Tìm kiếm
        var search = await Order.find({ name: { $regex: ".*" + req.query.search + ".*", $options: 'i' } });

        res.status(200).json({
            status: 'success',
            next: !req.query.page ? null : (order == "" ? null : `http://localhost:3000/api/orders?page=${parseInt(n)+1}`),
            previous: !req.query.page ? null : (parseInt(n) - 1 == 0 ? null : `http://localhost:3000/api/orders?page=${parseInt(n)-1}`),
            page: req.query.page,
            results: order.length,
            data: order,
            search
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.createOrder = async(req, res) => {
    try {
        if (!req.body.receiverName) {
            req.body.receiverName = req.body.customerName
        }

        if (!req.body.receiverPhone) {
            req.body.receiverPhone = req.body.customerPhone
        }

        const order = await Order.create(req.body);

        res.status(201).json({
            status: 'success',
            data: order
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.getOrder = async(req, res) => {
    try {
        const order = await Order.findById(req.params.id);

        if (!order) {
            return res.status(404).json({
                status: 'error',
                message: 'Order not found with that ID'
            })
        }

        res.status(200).json({
            status: 'success',
            data: order
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.updateOrder = async(req, res) => {
    try {
        const orderCheck = await Order.findById(req.params.id);

        let statusCheck = orderCheck.status

        let statusNew = req.body.status

        const order = await Order.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        })

        if (!order) {
            return res.status(404).json({
                status: 'error',
                message: 'Order not found with that ID'
            })
        }

        //CỘng lại số lượng nếu đơn hàng bị rejected--Cập nhật stock
        const orderItems = await OrderItems.find({
            orderID: order._id
        })

        //Chỉ được update số lượng trong kho khi status khác rejected
        if (statusCheck != 'Rejected' & statusNew == 'Rejected') {
            if (order.status == 'Rejected') {
                for (orderItem of orderItems) {
                    var product = await Product.findById(orderItem.productID)

                    product.quantity = product.quantity + orderItem.quantity

                    await product.save()
                }
            }
        }
        //End---CỘng lại số lượng nếu đơn hàng bị rejected--Cập nhật stock

        res.status(201).json({
            status: 'success',
            data: order
        })
    } catch (err) {
        console.log(err)
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.deleteOrder = async(req, res) => {
    try {
        const order = await Order.findByIdAndDelete(req.params.id);

        if (!order) {
            return res.status(404).json({
                status: 'error',
                message: 'Order not found with that ID'
            })
        }

        res.status(204).json({
            message: 'success',
            data: null
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}