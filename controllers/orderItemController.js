const OrderItem = require('../models/orderItemModel')
const Product = require('../models/productModel')

exports.getAllOrderItems = async(req, res) => {
    try {
        const orderItem = await OrderItem.find();

        res.status(200).json({
            status: 'success',
            results: orderItem.length,
            data: orderItem
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.createOrderItem = async(req, res) => {
    try {
        const product = await Product.findById(req.body.productID)

        req.body.price = req.body.quantity * product.price;

        const orderItem = await OrderItem.create(req.body);

        res.status(201).json({
            status: 'success',
            data: orderItem
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.getOrderItem = async(req, res) => {
    try {
        const orderItem = await OrderItem.findById(req.params.id);

        if (!orderItem) {
            res.status(404).json({
                status: 'error',
                message: 'Order item not found with that ID'
            })
        }

        res.status(200).json({
            status: 'success',
            data: orderItem
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.updateOrderItem = async(req, res) => {
    try {
        const orderItem = await OrderItem.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        })

        const product = await Product.findById(orderItem.productID)

        orderItem.price = orderItem.quantity * product.price;

        await orderItem.save();

        if (!orderItem) {
            return res.status(404).json({
                status: 'error',
                message: 'Order item not found with that ID'
            })
        }

        res.status(201).json({
            status: 'success',
            data: orderItem
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.deleteOrderItem = async(req, res) => {
    try {
        const orderItem = await OrderItem.findByIdAndDelete(req.params.id);

        if (!orderItem) {
            return res.status(404).json({
                status: 'error',
                message: 'Order item not found with that ID'
            })
        }

        res.status(204).json({
            status: 'success',
            data: null
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}