const User = require('../models/userModel')
const Cart = require('../models/cartModel')
const CartItem = require('../models/cartItemModel')
const Product = require('../models/productModel')
const Order = require('../models/orderModel')
const OrderItem = require('../models/orderItemModel')

const bcrypt = require('bcryptjs')

exports.getAllUsers = async(req, res) => {
    try {
        //Phân trang (pagination)
        if (!req.query.page) {
            var user = await User.find();
        } else {
            var n = req.query.page
            var x = 2 //Số lượng sp trên 1 trang
            user = await User.find().skip(n * x - x).limit(x)
        }
        //End--Phân trang (pagination)

        //Tìm kiếm
        var search = await User.find({ name: { $regex: ".*" + req.query.search + ".*", $options: 'i' } });

        res.status(200).json({
            status: 'success',
            next: !req.query.page ? null : (user == "" ? null : `http://localhost:3000/api/users?page=${parseInt(n)+1}`),
            previous: !req.query.page ? null : (parseInt(n) - 1 == 0 ? null : `http://localhost:3000/api/users?page=${parseInt(n)-1}`),
            page: req.query.page,
            results: user.length,
            data: user,
            search
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.createUser = async(req, res) => {
    try {
        if (req.body.password !== req.body.passwordConfirm) {
            return res.status(400).json({
                status: 'error',
                message: 'Passwords are not matching'
            })
        }

        if (req.body.username) {
            const user = await User.findOne({
                username: req.body.username
            })

            if (user) {
                return res.status(400).json({
                    status: 'error',
                    message: 'Username already exists'
                })
            }
        }

        if (req.body.email) {
            const user = await User.findOne({
                email: req.body.email
            })

            if (user) {
                return res.status(400).json({
                    status: 'error',
                    message: 'Email already exists'
                })
            }
        }

        let hashPassword = await bcrypt.hash(req.body.password, 12);

        const user = await User.create({
            name: req.body.name,
            username: req.body.username,
            email: req.body.email,
            password: hashPassword,
            passwordConfirm: req.body.passwordConfirm,
            phone: req.body.phone,
            birthday: req.body.birthday,
            address: req.body.address,
            role: req.body.role
        })

        user.passwordConfirm = undefined;

        await user.save();

        res.status(201).json({
            status: 'success',
            data: user
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.getUser = async(req, res) => {
    try {
        const user = await User.findById(req.params.id);

        if (!user) {
            return res.status(404).json({
                status: 'error',
                message: 'User not found with that ID'
            })
        }

        res.status(200).json({
            status: 'success',
            data: user
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.updateUser = async(req, res) => {
    try {
        if (req.body.password !== req.body.passwordConfirm) {
            return res.status(400).json({
                status: 'error',
                message: 'Passwords are not matching'
            })
        }

        if (req.body.username) {
            const user = await User.findOne({
                username: req.body.username
            })

            if (user) {
                return res.status(400).json({
                    status: 'error',
                    message: 'Username already exists'
                })
            }
        }

        if (req.body.email) {
            const user = await User.findOne({
                email: req.body.email
            })

            if (user) {
                return res.status(400).json({
                    status: 'error',
                    message: 'Email already exists'
                })
            }
        }

        if (req.body.password && req.body.passwordConfirm) {
            req.body.password = await bcrypt.hash(req.body.password, 12);
        }

        const user = await User.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        })

        if (!user) {
            return res.status(404).json({
                status: 'error',
                message: 'User not found with that ID'
            })
        }

        user.passwordConfirm = undefined;

        await user.save();

        res.status(201).json({
            status: 'success',
            data: user
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.deleteUser = async(req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);

        if (!user) {
            return res.status(404).json({
                status: 'error',
                message: 'User not found with that ID'
            })
        }

        res.status(204).json({
            status: 'success',
            data: null
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.addToCart = async(req, res) => {
    try {
        //Kiem tra customer da co cart chua
        let cart = await Cart.findOne({
            userID: req.user._id
        })

        //Chua thi se tao cart cho customer
        if (!cart) {
            cart = await Cart.create({
                userID: req.user._id
            })
        }

        //Bat dau them cart item cho cart
        req.body.cartID = cart._id

        //Kiem tra product co ton tai hay khong
        const product = await Product.findById(req.body.productID)

        if (!product) {
            return res.status(404).json({
                status: 'error',
                message: 'No product found with that ID'
            })
        }

        //Kiem tra san pham da duoc them bao cart chua
        const itemExists = await CartItem.findOne({
            productID: req.body.productID,
            cartID: cart._id
        })

        if (!itemExists) {
            var cartItem = await CartItem.create(req.body)
        }

        if (itemExists) {
            itemExists.quantity = itemExists.quantity + req.body.quantity;

            if (itemExists.quantity < 0) {
                return res.status(400).json({
                    status: 'error',
                    message: 'Quantity not enough'
                })
            }

            await itemExists.save()

            if (itemExists.quantity == 0) {
                const deleteCartItem = await CartItem.findByIdAndDelete({
                    productID: itemExists.productID
                })
            }
        }

        var cartItem = await CartItem.findOne({
            cartID: cart._id
        })

        res.status(201).json({
            status: 'success',
            data: {
                cart,
                cartItem
            }
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.checkout = async(req, res) => {
    try {
        if (req.user) {
            var cart = await Cart.findOne({
                userID: req.user._id
            })
        }

        if (!cart) {
            return res.status(404).json({
                status: 'error',
                message: 'Cart is empty'
            })
        }

        if (!req.body.receiverName) {
            req.body.receiverName = req.user.name
        }

        if (!req.body.receiverPhone) {
            req.body.receiverPhone = req.user.phone
        }

        var order = await Order.create({
            customerName: req.user.name,
            customerPhone: req.user.phone,
            receiverName: req.body.receiverName,
            receiverPhone: req.body.receiverPhone,
            shippingAddress: req.body.shippingAddress,
            description: req.body.description,
            userID: req.user._id,
            cartID: cart._id
        })

        var cartItems = await CartItem.find({
            cartID: cart._id
        })

        for (const cartItem of cartItems) {
            const product = await Product.findById(cartItem.productID)

            const orderItem = await OrderItem.create({
                orderID: order._id,
                productID: cartItem.productID,
                quantity: cartItem.quantity,
                price: product.price
            })

            product.quantity = product.quantity - orderItem.quantity

            if (product.quantity < 0) {
                return res.status(400).json({
                    status: 'error',
                    message: 'Product quantity is not enough'
                })
            }

            order.priceTotal = order.priceTotal + orderItem.price * orderItem.quantity

            order.productQuantity = order.productQuantity + orderItem.quantity

            await order.save()

            await product.save()
        }

        if (!req.user) {
            const order = await Order.create(req.body)
        }

        const deleteCart = await Cart.findOneAndDelete(cart._id)

        const deleteCartItem = await CartItem.deleteMany({
            cartID: cart._id
        })

        const accountSid = 'ACfd35588805651bbf5aeea5e6aec38e85';
        const authToken = '5895082f198c681a66b4e0d3e0c21909';
        const client = require('twilio')(accountSid, authToken);

        //Gửi tin nhắn đã đặt hàng thành công
        client.messages
            .create({
                body: `
Cảm ơn ${req.user.name} đã mua hàng tại Flower Shop ❤️
Mã đơn hàng: ${order._id}
Chúc bạn và người nhận luôn vui vẻ !
`,
                from: '+19547371820',
                to: `+84${(req.user.phone).slice(1)}`
            })
            .then(message => console.log(message.sid));
        //End -- Gửi tin nhắn đã đặt hàng thành công

        res.status(201).json({
            status: 'success',
            data: order
        })
    } catch (err) {
        console.log(err)
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}