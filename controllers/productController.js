const Product = require('../models/productModel');

const cloudinary = require('cloudinary').v2
const fs = require('fs')

cloudinary.config({
    cloud_name: 'vulythienanh',
    api_key: '711852792357315',
    api_secret: 'zxPG9EpsCwqF4n1bRV_pGV4vkbQ'
})

exports.getAllProducts = async(req, res) => {
    try {
        //Phân trang (pagination)
        if (!req.query.page) {
            var product = await Product.find();
        } else {
            var n = req.query.page
            var x = 2 //Số lượng sp trên 1 trang
            product = await Product.find().skip(n * x - x).limit(x)
        }
        //End--Phân trang (pagination)

        //Tìm kiếm
        var search = await Product.find({ name: { $regex: ".*" + req.query.search + ".*", $options: 'i' } });

        res.status(200).json({
            status: 'success',
            next: !req.query.page ? null : (product == "" ? null : `http://localhost:3000/api/products?page=${parseInt(n)+1}`),
            previous: !req.query.page ? null : (parseInt(n) - 1 == 0 ? null : `http://localhost:3000/api/products?page=${parseInt(n)-1}`),
            page: req.query.page,
            results: product.length,
            data: product,
            search
        })
    } catch (err) {
        console.log(err)
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.createProduct = async(req, res) => {
    try {
        if (req.files) {
            //Kiểm tra định dạng file upload
            if (!req.files.imageCover.mimetype.startsWith('image/')) {
                return res.status(404).json({
                    status: 'error',
                    message: 'Image Cover must be an image'
                })
            }

            //Kiểm tra định dang các file format
            for (let image of req.files.images) {
                if (!image.mimetype.startsWith('image/')) {
                    return res.status(404).json({
                        status: 'error',
                        message: 'Images must be images'
                    })
                }
            }

            //Upload imageCover lên cloudinary
            const file = req.files.imageCover;
            var uploadImgCover = await cloudinary.uploader.upload(file.tempFilePath, function(err, result) {
                console.log("Error", err);
                console.log("Success", result);
            })

            //Dán link imageCover sau khi upload vào req.body để lưu vào database
            if (uploadImgCover) {
                req.body.imageCover = uploadImgCover.secure_url
            }

            //Tạo mảng lưu các link file temp
            imgArrayUploads = req.files.images.map(image => image.tempFilePath)

            //Upload các image lên cloudinary
            url_array = []
            for (let image of imgArrayUploads) {
                var imgUploads = await cloudinary.uploader.upload(image, function(err, result) {
                    console.log("Error", err);
                    console.log("Success", result);
                });

                //Thêm link vào mảng
                url_array.push(imgUploads.secure_url)
            }

            //Lưu link lại vào database
            if (url_array.length > 0) {
                req.body.images = url_array
            }

            //Xóa file temp trong tmp của imageCover
            fs.unlinkSync(req.files.imageCover.tempFilePath)

            //Xóa file temp trong tmp của images
            for (let delImg of imgArrayUploads) {
                fs.unlinkSync(delImg)
            }
        }

        const product = await Product.create(req.body);

        res.status(201).json({
            status: 'success',
            data: product
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.getProduct = async(req, res) => {
    try {
        const product = await Product.findById(req.params.id);

        if (!product) {
            return res.status(404).json({
                status: 'error',
                message: 'Product not found with that ID'
            })
        }

        res.status(200).json({
            status: 'success',
            data: product
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.updateProduct = async(req, res) => {
    try {
        if (req.files) {
            //Kiểm tra định dạng file upload
            if (!req.files.imageCover.mimetype.startsWith('image/')) {
                return res.status(404).json({
                    status: 'error',
                    message: 'Image Cover must be an image'
                })
            }

            //Kiểm tra định dang các file format
            for (let image of req.files.images) {
                if (!image.mimetype.startsWith('image/')) {
                    return res.status(404).json({
                        status: 'error',
                        message: 'Images must be images'
                    })
                }
            }

            //Upload imageCover lên cloudinary
            const file = req.files.imageCover;
            var uploadImgCover = await cloudinary.uploader.upload(file.tempFilePath, function(err, result) {
                console.log("Error", err);
                console.log("Success", result);
            })

            //Dán link imageCover sau khi upload vào req.body để lưu vào database
            if (uploadImgCover) {
                req.body.imageCover = uploadImgCover.secure_url
            }

            //Tạo mảng lưu các link file temp
            imgArrayUploads = req.files.images.map(image => image.tempFilePath)

            //Upload các image lên cloudinary
            url_array = []
            for (let image of imgArrayUploads) {
                var imgUploads = await cloudinary.uploader.upload(image, function(err, result) {
                    console.log("Error", err);
                    console.log("Success", result);
                });

                //Thêm link vào mảng
                url_array.push(imgUploads.secure_url)
            }

            //Lưu link lại vào database
            if (url_array.length > 0) {
                req.body.images = url_array
            }

            //Xóa file temp trong tmp của imageCover
            fs.unlinkSync(req.files.imageCover.tempFilePath)

            //Xóa file temp trong tmp của images
            for (let delImg of imgArrayUploads) {
                fs.unlinkSync(delImg)
            }
        }

        const product = await Product.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        })

        if (!product) {
            return res.status(404).json({
                status: 'error',
                message: 'Product not found with that ID'
            })
        }

        res.status(200).json({
            status: 'success',
            data: product
        })
    } catch (err) {
        console.log(err)
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.deleteProduct = async(req, res) => {
    try {
        const product = await Product.findByIdAndDelete(req.params.id);

        if (!product) {
            return res.status(404).json({
                status: 'error',
                message: 'Product not found with that ID'
            })
        }

        res.status(204).json({
            status: 'success',
            data: null
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}