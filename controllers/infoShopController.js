const InfoShop = require('../models/infoShopModel');

exports.getInfoShop = async(req, res) => {
    try {
        const infoShop = await InfoShop.find();

        res.status(200).json({
            status: 'success',
            data: infoShop
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.createInfoShop = async(req, res) => {
    try {
        const checkInfo = await InfoShop.find();

        if (checkInfo != "") {
            return res.status(400).json({
                status: 'error',
                message: 'Info Shop already exists'
            })
        }

        const infoShop = await InfoShop.create(req.body)

        res.status(201).json({
            status: 'success',
            data: infoShop
        })
    } catch (err) {
        console.log(err)
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}

exports.updateInfoShop = async(req, res) => {
    try {
        const infoShop = await InfoShop.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        })

        if (!infoShop) {
            return res.status(404).json({
                status: 'error',
                message: 'You must create Info Shop before updating'
            })
        }

        return res.status(200).json({
            status: 'success',
            data: infoShop
        })
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}