const User = require('../models/userModel')
const Order = require('../models/orderModel')
const Product = require('../models/productModel')

const moment = require('moment')
const { Types } = require('mongoose')

exports.dashboard = async(req, res) => {
    try {
        const customer = await User.find({ role: 'customer' })

        if (req.query.category) {
            var productDetails = await Product.aggregate([
                { $match: { categoryID: Types.ObjectId(req.query.category) } },
                { $group: { _id: null, totalInStock: { $sum: '$quantity' } } },
                { $addFields: { category: req.query.category } }
            ])
        } else {
            var productDetails = await Product.aggregate([
                { $group: { _id: null, totalInStock: { $sum: '$quantity' } } },
                { $addFields: { Category: 'All Category' } }
            ])
        }

        const orderDetails = await Order.aggregate([
            { $match: { status: 'Completed' } },
            {
                $group: {
                    _id: null,
                    totalRevenue: { $sum: '$priceTotal' },
                    totalSoldProducts: { $sum: '$productQuantity' }
                }
            }
        ])

        //month Revenue 
        const orders = await Order.find({ status: 'Completed' })

        const totalRevenueMonth = orders.filter(function(x) {
            timeOrders = moment(x.createdAt).locale('vi').format('L').split('/')
            timeOrders.shift()

            if (req.query.month & req.query.year) {
                return (timeOrders[0] === req.query.month & timeOrders[1] === req.query.year)
            }

            if (req.query.month) {
                return (timeOrders[0] === req.query.month)
            }

            if (req.query.year) {
                return (timeOrders[1] === req.query.year)
            }

            return (timeOrders)
        })

        const resultRevenue = totalRevenueMonth.reduce(function(a, b) {
            return a + b.priceTotal
        }, 0);

        const resultSoldProducts = totalRevenueMonth.reduce(function(a, b) {
            return a + b.productQuantity
        }, 0);
        //end -- month Revenue 

        const orderdetails = await Order.aggregate([
            { $match: { status: "Completed" } }, {
                $lookup: {
                    from: "orderitems",
                    localField: '_id',
                    foreignField: 'orderID',
                    as: 'details'
                }
            }
        ])

        const bestSeller = orderdetails.filter((x) => {
            timeOrders = moment(x.createdAt).locale('vi').format('L').split('/')
            timeOrders.shift()

            if (req.query.month & req.query.year) {
                return (timeOrders[0] === req.query.monthBest & timeOrders[1] === req.query.yearBest)
            }

            if (req.query.month) {
                return (timeOrders[0] === req.query.monthBest)
            }

            if (req.query.year) {
                return (timeOrders[1] === req.query.yearBest)
            }

            return (timeOrders)
        })

        var array = []
        for (orderdetail of bestSeller) {
            for (a of orderdetail.details) {
                array.push(a)
            }
        }

        const topProducts = Object.values(array.reduce((acc, { productID, quantity }) => {
            acc[productID] = { productID, quantity: (acc[productID] ? acc[productID].quantity : 0) + quantity };
            return acc;
        }, {}));

        topProducts.sort((a, b) => { return b.quantity - a.quantity });

        res.status(200).json({
            status: 'success',
            time: moment().locale('vi').format('L LTS'),
            totalCustomer: customer.length,
            productDetails,
            orderDetails,
            revenue: {
                month: req.query.month,
                year: req.query.year,
                resultRevenue,
                resultSoldProducts
            },
            bestSeller: {
                monthBest: req.query.monthBest,
                yearBest: req.query.yearBest,
                topProducts
            }
        })
    } catch (err) {
        console.log(err)
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        })
    }
}