const router = require('express').Router();

const { getAllCartItems, createCartItem, getCartItem, updateCartItem, deleteCartItem } = require('../controllers/cartItemController');

const { authenticate, restrictTo } = require('../controllers/authController');

router.route('/')
    .get(authenticate, restrictTo('admin', 'staff'), getAllCartItems)
    .post(authenticate, restrictTo('admin', 'staff'), createCartItem)

router.route('/:id')
    .get(authenticate, restrictTo('admin', 'staff'), getCartItem)
    .patch(authenticate, restrictTo('admin', 'staff'), updateCartItem)
    .delete(authenticate, restrictTo('admin', 'staff'), deleteCartItem)

module.exports = router