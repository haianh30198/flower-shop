const router = require('express').Router();
const fileupload = require('express-fileupload')

// File upload
router.use(fileupload({
    useTempFiles: true
}));

const { getAllCategories, createCategory, getCategory, updateCategory, deleteCategory } = require('../controllers/categoryController.js');

const { authenticate, restrictTo } = require('../controllers/authController');

router.route('/')
    .get(getAllCategories)
    .post(authenticate, restrictTo('admin', 'staff'), createCategory)

router.route('/:id')
    .get(getCategory)
    .patch(authenticate, restrictTo('admin', 'staff'), updateCategory)
    .delete(authenticate, restrictTo('admin', 'staff'), deleteCategory)

module.exports = router;