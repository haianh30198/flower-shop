const router = require('express').Router();

const { getAllOrderItems, createOrderItem, getOrderItem, updateOrderItem, deleteOrderItem } = require('../controllers/orderItemController')

const { authenticate, restrictTo } = require('../controllers/authController');

router.route('/')
    .get(authenticate, restrictTo('admin', 'staff'), getAllOrderItems)
    .post(authenticate, restrictTo('admin', 'staff'), createOrderItem)

router.route('/:id')
    .get(authenticate, restrictTo('admin', 'staff'), getOrderItem)
    .patch(authenticate, restrictTo('admin', 'staff'), updateOrderItem)
    .delete(authenticate, restrictTo('admin', 'staff'), deleteOrderItem)

module.exports = router