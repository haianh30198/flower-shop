const router = require('express').Router();

const { getInfoShop, createInfoShop, updateInfoShop } = require('../controllers/infoShopController')

const { authenticate, restrictTo } = require('../controllers/authController');

router.route('/')
    .get(authenticate, restrictTo('admin', 'staff'), getInfoShop)
    .post(authenticate, restrictTo('admin', 'staff'), createInfoShop)

router.route('/:id')
    .patch(authenticate, restrictTo('admin', 'staff'), updateInfoShop)

module.exports = router;