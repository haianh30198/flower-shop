const router = require('express').Router();

const { getAllOrders, createOrder, getOrder, updateOrder, deleteOrder } = require('../controllers/orderController');

const { authenticate, restrictTo } = require('../controllers/authController');

router.route('/')
    .get(authenticate, restrictTo('admin', 'staff'), getAllOrders)
    .post(authenticate, restrictTo('admin', 'staff'), createOrder)

router.route('/:id')
    .get(getOrder)
    .patch(authenticate, restrictTo('admin', 'staff'), updateOrder)
    .delete(authenticate, restrictTo('admin', 'staff'), deleteOrder)

module.exports = router;