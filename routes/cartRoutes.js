const router = require('express').Router();

const { getAllCarts, createCart, getCart, updateCart, deleteCart } = require('../controllers/cartController')

const { authenticate, restrictTo } = require('../controllers/authController');

router.route('/')
    .get(authenticate, restrictTo('admin', 'staff'), getAllCarts)
    .post(authenticate, restrictTo('admin', 'staff'), createCart)

router.route('/:id')
    .get(authenticate, restrictTo('admin', 'staff'), getCart)
    .patch(authenticate, restrictTo('admin', 'staff'), updateCart)
    .delete(authenticate, restrictTo('admin', 'staff'), deleteCart)

module.exports = router;