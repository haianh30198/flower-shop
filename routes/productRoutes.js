const router = require('express').Router();
const fileupload = require('express-fileupload')

// File upload
router.use(fileupload({
    useTempFiles: true
}));

const { getAllProducts, createProduct, getProduct, updateProduct, deleteProduct } = require('../controllers/productController.js');

const { authenticate, restrictTo } = require('../controllers/authController');

router.route('/')
    .get(getAllProducts)
    .post(authenticate, restrictTo('admin', 'staff'), createProduct)

router.route('/:id')
    .get(getProduct)
    .patch(authenticate, restrictTo('admin', 'staff'), updateProduct)
    .delete(authenticate, restrictTo('admin', 'staff'), deleteProduct)

module.exports = router;