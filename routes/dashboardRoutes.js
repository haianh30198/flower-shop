const router = require('express').Router();

const { dashboard } = require('../controllers/dashboardController')

const { authenticate, restrictTo } = require('../controllers/authController');

router.route('/')
    .get(authenticate, restrictTo('admin', 'staff'), dashboard)

module.exports = router