const router = require('express').Router();

const { getAllUsers, createUser, getUser, updateUser, deleteUser, addToCart, checkout } = require('../controllers/userController');
const { signup, signin, signout, authenticate, restrictTo, forgotPassword, changePassword } = require('../controllers/authController');

router.route('/')
    .get(authenticate, restrictTo('admin', 'staff'), getAllUsers)
    .post(createUser)

router.route('/:id')
    .get(authenticate, restrictTo('admin', 'staff'), getUser)
    .patch(authenticate, restrictTo('admin', 'staff', 'customer'), updateUser)
    .delete(authenticate, restrictTo('admin', 'staff'), deleteUser)

router.route('/signup')
    .post(signup)

router.route('/signin')
    .post(signin)

router.route('/signout')
    .post(signout)

router.route('/add-to-cart')
    .post(authenticate, addToCart)

router.route('/checkout')
    .post(authenticate, checkout)

router.route('/forgot-password')
    .post(forgotPassword)

router.route('/change-password')
    .post(changePassword)

module.exports = router;