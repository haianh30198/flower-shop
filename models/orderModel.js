const { Schema, model } = require('mongoose')
const validator = require('validator')

const orderSchema = new Schema({
    customerName: {
        type: String,
        trim: true,
        require: [true, 'Order must have customer name'],
        minLength: [5, 'A name must be longer than 5 characters'],
        maxLength: [100, 'A name must be less than 100 characters']
    },

    customerPhone: {
        type: String,
        trim: true,
        require: [true, 'A user must have a phone number'],
        validate: [validator.isMobilePhone, 'vi-VN', 'Phone number must use in Vietnam']
    },

    receiverName: {
        type: String,
        trim: true,
        require: [true, 'Order must have receiver name'],
        minLength: [5, 'A name must be longer than 5 characters'],
        maxLength: [100, 'A name must be less than 100 characters']
    },

    receiverPhone: {
        type: String,
        trim: true,
        require: [true, 'A user must have a phone number'],
        validate: [validator.isMobilePhone, 'vi-VN', 'Phone number must use in Vietnam']
    },

    shippingAddress: {
        type: String,
        trim: true,
        require: [true, 'Order must have shipping address'],
        minLength: [5, 'Please provide a valid shipping address'],
        maxLength: [255, 'Shipping address must be less than 255 characters']
    },

    description: {
        type: String,
        trim: true,
        maxLength: [255, 'Description must be longer than 255 characters'],
        default: 'Have nice a day'
    },

    productQuantity: {
        type: Number,
        require: [true, 'Order must be a product quantity'],
        min: [0, 'Order must be a product quantity'],
        validate: [Number.isInteger, 'Please provide an integer number'],
        default: 0
    },

    priceTotal: {
        type: Number,
        require: [true, 'Order must be have a price total'],
        min: [0, 'Price total must be unsigned integer number'],
        validator: [Number.isInteger, 'Please provide an integer number'],
        default: 0
    },

    status: {
        type: String,
        enum: ['No action', 'Accepted', 'Rejected', 'Shipping', 'Completed'],
        default: 'No action'
    },

    createdAt: {
        type: Date,
        default: Date.now
    },

    updateAt: {
        type: Date,
        default: Date.now
    },

    userID: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        require: [true, 'Order must belong to user'],
        default: null
    },

    cartID: {
        type: Schema.Types.ObjectId,
        ref: 'Cart',
        require: [true, 'Order must belong to cart'],
        default: null
    }
})

//Cập nhật lại updateAt
orderSchema.pre('findOneAndUpdate', function(next) {
    const additionalUpdates = { updatedAt: Date.now() }

    this.set(additionalUpdates)

    next()
})

const Order = model('Order', orderSchema);

module.exports = Order;