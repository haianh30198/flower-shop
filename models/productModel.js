const { Schema, model } = require('mongoose');
const slugify = require('slugify')

const productSchema = new Schema({
    name: {
        type: String,
        unique: true,
        trim: true,
        require: [true, 'A product must have a name'],
        minLength: [5, 'A product name must be longer than 5 characters'],
        maxLength: [100, 'A product name must be less than 100 characters']
    },

    slug: String,

    price: {
        type: Number,
        default: 0,
        require: [true, 'A product must have a price']
    },

    quantity: {
        type: Number,
        default: 0
    },

    description: {
        type: String,
        trim: true,
        require: [true, 'A product must have a description'],
        minLength: [5, 'A product description must be longer than 5 characters'],
        maxLength: [255, 'A product description must be less than 255 characters']
    },

    imageCover: String,

    images: [String],

    status: {
        type: String,
        enum: ['Actived', 'Inactived'],
        default: 'Actived'
    },

    expiryDate: {
        type: Date
    },

    createdAt: {
        type: Date,
        default: Date.now
    },

    updatedAt: {
        type: Date,
        default: Date.now
    },

    categoryID: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        require: [true, 'Product must belong to a category']
    }
})

//Slug name
productSchema.pre('save', function(next) {
    this.slug = slugify(this.name, {
        lower: true,
        locale: 'vi'
    }) + '-' + Date.now()

    next();
})

//Cập nhật lại updateAt và slug khi đổi tên
productSchema.pre('findOneAndUpdate', function(next) {
    const name = this._update.name;
    const additionalUpdates = name ? {
        slug: slugify(name, {
            lower: true,
            locale: 'vi'
        }) + '-' + Date.now(),
        updatedAt: Date.now()
    } : {
        updatedAt: Date.now()
    }

    this.set(additionalUpdates);

    next();
})

const Product = model('Product', productSchema);

module.exports = Product;