const { Schema, model } = require('mongoose');
const validator = require('validator')

const infoShopSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: [true, 'The shop must have a name'],
        minLength: [5, 'The shop name must be longer than 5 characters'],
        maxLength: [100, 'The shop name must be less than 100 characters'],
        default: 'Flower Shop'
    },

    slogan: {
        type: String,
        trim: true,
        required: [true, 'The shop must have a slogan'],
        minLength: [5, 'The shop name must be longer than 5 characters'],
        maxLength: [255, 'The shop name must be less than 255 characters'],
        default: 'Hoa đẹp thay lời yêu thương'
    },

    logo: {
        type: String,
        default: '🌸'
    },

    phone: {
        type: String,
        trim: true,
        require: [true, 'The shop must have a phone number'],
        validate: [validator.isMobilePhone, 'vi-VN', 'Phone number must use in Vietnam'],
        default: '0335832168'
    },

    email: {
        type: String,
        unique: true,
        trim: true,
        lowercase: true,
        require: [true, 'The shop must have a email'],
        minLength: [5, 'The shop email must be longer than 5 characters'],
        validate: [validator.isEmail, 'The value is not a valid email'],
        default: 'haianh30198@gmail.com'
    },

    socialNetwork: {
        type: [String],
        default: ['https://www.facebook.com/vulythienanh', 'https://www.instagram.com/vlta_98']
    },

    status: {
        type: String,
        enum: ['Actived', 'Inactived'],
        default: 'Actived'
    },

    createdAt: {
        type: Date,
        default: Date.now
    },

    updatedAt: {
        type: Date,
        default: Date.now
    }
})

//Cập nhật lại updateAt
infoShopSchema.pre('findOneAndUpdate', function(next) {
    const additionalUpdates = { updatedAt: Date.now() }
    this.set(additionalUpdates)

    next();
})

const InfoShop = model('InfoShop', infoShopSchema);

module.exports = InfoShop;