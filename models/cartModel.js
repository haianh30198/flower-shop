const { Schema, model } = require('mongoose');

const cartSchema = new Schema({
    createdAt: {
        type: Date,
        default: Date.now
    },

    updatedAt: {
        type: Date,
        default: Date.now
    },

    userID: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        unique: true,
    }
})

//Cập nhật lại updateAt
cartSchema.pre('findOneAndUpdate', function(next) {
    const additionalUpdates = { updatedAt: Date.now() }

    this.set(additionalUpdates)

    next()
})

const Cart = model('Cart', cartSchema)

module.exports = Cart;