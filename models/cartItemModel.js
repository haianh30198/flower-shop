const { Schema, model } = require('mongoose');
const validator = require('validator')

const cartItemSchema = new Schema({
    productID: {
        type: Schema.Types.ObjectId,
        ref: 'Product',
        require: [true, 'CartItem must belong to a Product']
    },

    cartID: {
        type: Schema.Types.ObjectId,
        ref: 'Cart',
        require: [true, 'CartItem must belong to a Cart']
    },

    quantity: {
        type: Number,
        require: [true, 'Product must have quantity'],
        min: [0, 'Product quantity must be unsigned integer number'],
        validate: [Number.isInteger, 'Please provide an integer number']
    }
})

//Kiểm tra tồn tài của product
// cartItemSchema.pre('/^find/', function(next) {
//     this.populate('productID');

//     next()
// })

const CartItem = model('CartItem', cartItemSchema)

module.exports = CartItem