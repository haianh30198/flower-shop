const { Schema, model } = require('mongoose');

const orderItemSchema = new Schema({
    orderID: {
        type: Schema.Types.ObjectId,
        ref: 'Order',
        required: [true, 'Order Item must belong to Order']
    },

    productID: {
        type: Schema.Types.ObjectId,
        ref: 'Product',
        required: [true, 'Order Item must have a Product']
    },

    quantity: {
        type: Number,
        min: 0,
        max: [99, 'You want to buy big quantity. Please, call store'],
        require: [true, 'Product must have a quantity']
    },

    price: {
        type: Number,
        required: [true, 'Product must have a price']
    }
})

const OrderItem = model('OrderItem', orderItemSchema);

module.exports = OrderItem;