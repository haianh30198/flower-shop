const { Schema, model } = require('mongoose');
const slugify = require('slugify');

const categorySchema = new Schema({
    name: {
        type: String,
        unique: true,
        trim: true,
        require: [true, 'A category must have a name'],
        minLength: [5, 'A category name must be longer than 5 characters'],
        maxLength: [100, 'A category name must be less than 100 characters']
    },

    slug: String,

    description: {
        type: String,
        trim: true,
        require: [true, 'A category must have a description'],
        minLength: [5, 'A category description must be longer than 5 characters'],
        maxLength: [255, 'A category description must be less than 255 characters']
    },

    imageCover: String,

    status: {
        type: String,
        enum: ['Actived', 'Inactived'],
        default: 'Actived'
    },

    createdAt: {
        type: Date,
        default: Date.now,
    },

    updatedAt: {
        type: Date,
        default: Date.now,
    }
})

//Slug name
categorySchema.pre('save', function(next) {
    this.slug = slugify(this.name, {
        lower: true,
        locale: 'vi'
    });

    next();
})

//Cập nhật lại updateAt và slug khi đổi tên
categorySchema.pre('findOneAndUpdate', function(next) {
    const name = this._update.name;
    const additionalUpdates = name ? {
        slug: slugify(name, {
            lower: true,
            locale: 'vi'
        }),
        updatedAt: Date.now()
    } : {
        updatedAt: Date.now()
    }

    this.set(additionalUpdates);

    next();
})

const Category = model('Category', categorySchema);

module.exports = Category;