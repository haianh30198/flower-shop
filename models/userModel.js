const { Schema, model } = require('mongoose');
const validator = require('validator')

const userSchema = new Schema({
    name: {
        type: String,
        trim: true,
        require: [true, 'A user must have a name'],
        minLength: [5, 'A user name must be longer than 5 characters'],
        maxLength: [100, 'A user name must be less than 100 characters']
    },

    username: {
        type: String,
        trim: true,
        lowercase: true,
        require: [true, 'A user must have a username'],
        minLength: [5, 'A user username must be longer than 5 characters'],
        maxLength: [100, 'A user username must be less than 100 characters']
    },

    email: {
        type: String,
        unique: true,
        trim: true,
        lowercase: true,
        require: [true, 'A user must have a email'],
        minLength: [5, 'A user email must be longer than 5 characters'],
        validate: [validator.isEmail, 'The value is not a valid email']
    },

    password: {
        type: String,
        require: [true, 'a user must have a password'],
        minLength: [5, 'The value must be longer than 5 characters'],
        validate: {
            validator: (value) => {
                return !value.includes(' ')
            },
            message: 'The password can not contain spaces'
        }
    },

    passwordConfirm: String,

    phone: {
        type: String,
        trim: true,
        require: [true, 'A user must have a phone number'],
        validate: [validator.isMobilePhone, 'vi-VN', 'Phone number must use in Vietnam']
    },

    birthday: Date,

    address: {
        type: String,
        trim: true,
        require: [true, 'Please enter a valid address'],
        minLength: [5, 'Address must be longer than 5 characters']
    },

    role: {
        type: String,
        enum: ['admin', 'staff', 'customer', 'shipper'],
        default: 'customer'
    },

    status: {
        type: String,
        enum: ['Actived', 'Inactived'],
        default: 'Actived'
    },

    createdAt: {
        type: Date,
        default: Date.now
    },

    updatedAt: {
        type: Date,
        default: Date.now
    }
})

//Cập nhật lại updateAt
userSchema.pre('findOneAndUpdate', function(next) {
    const additionalUpdates = { updatedAt: Date.now() }
    this.set(additionalUpdates)

    next();
})

const User = model('User', userSchema);

module.exports = User;