const express = require('express');
const mongoose = require('mongoose')
const app = express();
const port = process.env.PORT || 3000;

const categoryRouter = require('./routes/catogoryRoutes');
const productRouter = require('./routes/productRoutes');
const userRouter = require('./routes/userRoutes');
const cartRouter = require('./routes/cartRoutes');
const cartItemRouter = require('./routes/cartItemRoutes');
const orderRouter = require('./routes/orderRoutes');
const orderItemRouter = require('./routes/orderItemRoutes');
const dashboardRouter = require('./routes/dashboardRoutes');
const infoShopRouter = require('./routes/infoShopRoutes');

mongoose.connect('mongodb://localhost:27017/flower-shop', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
}).then(_ => {
    console.log('Connected to MongoDB successfully');
})

//Body parser
app.use(express.json());

app.use('/api/categories', categoryRouter)
app.use('/api/products', productRouter)
app.use('/api/users', userRouter)
app.use('/api/carts', cartRouter)
app.use('/api/cartitems', cartItemRouter)
app.use('/api/orders', orderRouter)
app.use('/api/orderItems', orderItemRouter)
app.use('/api/dashboard', dashboardRouter)
app.use('/api/infoshop', infoShopRouter)

app.listen(port, () => console.log(`Server listening at http://localhost:${port}`))